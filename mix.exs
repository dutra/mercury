defmodule Mercury.MixProject do
  use Mix.Project

  def project do
    [
      app: :mercury,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :cowboy, :plug,
                           :timex, :sqlite_ecto2, :ecto],
      mod: {Mercury, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
      {:poison, "~> 3.1"},
      {:cowboy, "~> 1.0.0"},
      {:plug, "~> 1.0"},
      {:timex, "~> 3.1"},
      {:nanoid, "~> 1.0.1"},
      {:sqlite_ecto2, "~> 2.2"},
      {:ecto, "~> 2.2"}
    ]
  end
end
