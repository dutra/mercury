defmodule Repo.Migrations.CreateTrack do
  use Ecto.Migration

  def change do
    create table :track do

      add :nanoid, :string
      add :ipaddr, :string
      add :browser, :string
      add :date, :utc_datetime

    end
  end
end
