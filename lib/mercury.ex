defmodule Mercury do
  @moduledoc """
  Documentation for Mercury.
  """

  use Application

  def start(_type, _args) do
    Mercury.Supervisor.start_link(name: Mercury.Supervisor)
  end

  @doc """
  Hello world.

  ## Examples

      iex> Mercury.hello
      :world

  """
  def hello do
    :world
  end
end
