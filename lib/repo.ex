defmodule Repo do
  use Ecto.Repo,
    otp_app: :mercury,
    adapter: Sqlite.Ecto2
end

defmodule Track do
  use Ecto.Schema

  schema "track" do
    field :nanoid, :string     # Defaults to type :string
    field :ipaddr, :string
    field :browser, :string
    field :date, :utc_datetime
  end
end
