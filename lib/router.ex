defmodule Mercury.Router do
  use Plug.Router
  require Ecto.Query

  @gif_data <<71, 73, 70, 56, 57, 97, 1, 0, 1, 0, -128, 0,
    0, -1, -1, -1, 0, 0, 0, 33, -7, 4, 1, 0, 0, 0, 0, 44, 0,
    0, 0, 0, 1, 0, 1, 0, 0, 2, 2, 68, 1, 0, 59>>

  plug Plug.Logger
  plug :match
  plug Plug.Parsers, parsers: [:json],
    pass:  ["application/json"],
    json_decoder: Poison
  plug :dispatch

  get "/generate" do
    nanoid = Nanoid.generate()
    send_resp(conn, 200, nanoid)
  end

  get "/info/:nanoid" do
    tracks = Track |>
      Ecto.Query.where(nanoid: ^nanoid) |>
      Repo.all |>
      Enum.map(fn t -> Map.take(t, [:nanoid, :ipaddr, :browser, :date]) end)

    IO.inspect tracks
    send_resp(conn, 200, Poison.encode!(tracks))
  end

  get "/gif/:full" do
    IO.inspect conn
    regex = ~r/(?<nanoid>[A-Za-z0-9_~]+)\.gif/

    %{"nanoid" => nanoid} = Regex.named_captures(regex, full)

    {"user-agent", agent} = conn.req_headers |>
      Enum.filter(fn {key, _} -> key == "user-agent" end) |>
      List.first

    track = %Track{
      nanoid: nanoid,
      ipaddr: conn.remote_ip |> Tuple.to_list |> Enum.join("."),
      browser: agent,
      date: DateTime.utc_now()
    }
    {:ok, _} = Repo.insert(track)

    put_resp_header(conn, "Content-Type", "image/gif")
    send_resp(conn, 200, @gif_data)
    # send_resp(conn, 200, "route after /gif: #{nanoid}")
  end
end
