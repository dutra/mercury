defmodule Mercury.Supervisor do
  use Supervisor

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    children = [
      Plug.Adapters.Cowboy.child_spec(scheme: :http, plug: Mercury.Router, options: [port: 4001]),
      Repo
      ]

    Supervisor.init(children, strategy: :one_for_one, name: Mercury.Supervisor)

  end
end
