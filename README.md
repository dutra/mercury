# Mercury

Mercury is capable of generating uniquely identified GIFs that are invisible.
Add the generated GIF to an email, and once the email is opened by recipient's email client,
a request will be made to fetch the GIF. Mercury will log who made the request.
By logging the request information including the uniquely generated identifier, it is possible to figure out by who and when your email was opened.

## Available Routes

### /generate
Returns an unique id. Please include the image http://\<base_url>/gif/\<uniqueid>.gif

### /info/\<id>
Retrieves requests made for gif identified by \<id>

### /gif/\<id>.gif
Retrieves a 1x1 pixel transparent GIF and logs the request with remote ip and browser information
